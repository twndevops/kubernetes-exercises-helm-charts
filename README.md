#### Notes for "Module 10 - Container Orchestration with Kubernetes" exercises

- EXERCISES' GOAL: configure and deploy Java-Gradle app manually to K8s cluster before automating.

EXERCISE 1: Create a Kubernetes cluster (Minikube or LKE)

- Created `k8s-exercises` cluster on Linode w/ 2 4 GB worker nodes b/c will have 2+ replicas of DB and app (ideally, the replicas will be spread across the nodes).

- Downloaded cluster's `*kubconfig.yaml` to repo. Exported KUBECONFIG=`*kubconfig.yaml` to locally connect to remote hosted cluster.

- Checked connection was established by running `kubectl get nodes`. NOTE that `kubectl` and `helm` cmds execute against the currently connected cluster.

- SMART PRACTICE GOING FORWARD: configure all components into a working cluster in the default NS before doing so in a dedicated NS. Cmds will be less verbose and configs will be easier to debug.
  - If you want to deploy the cluster in a dedicated NS, declare `namespace` property in each component's `metadata` block.

EXERCISE 2: Deploy Mysql database with 2 replicas and volumes for data persistence. Simplify the process by using Helm.

- Must deploy DB using StatefulSet, not Deployment component! Since a DB sts requires more configuration details (ex. for syncing replicas), easier to use Helm chart.

  - Found [MySQL chart managed by Bitnami](https://artifacthub.io/packages/helm/bitnami/mysql?modal=install) on ArtifactHub.
  - Added Bitnami repo: `helm repo add bitnami https://charts.bitnami.com/bitnami`
  - Before installing chart, set params to override sts defaults in `mysqldb.yaml`.
    - Referenced [Bitnami repo Git docs](https://github.com/bitnami/charts/tree/main/bitnami/mysql) for MySQL chart Parameters.

- Installed the chart passing the overrides: `helm install mysql --values mysqldb.yaml bitnami/mysql`. `mysql`= release name AND sts name.

  - WAIT A WHILE FOR THE PODS TO START RUNNING. `kubectl get all` shows `mysql-primary-0` 0/1, `mysql-secondary-0` 0/1, and `mysql-secondary-1` 0/1 but also that all pods are RUNNING. If Status is not an Error, WAIT.

  - Headless svc's enable data syncing b/w main DB pod and replicas.
  - Ran `kubectl get pod -o wide` to get pod IPs and `kubectl describe svc mysql-secondary` to confirm:

    - secondary sts's pod IPs are svc endpoints
    - pods are listening on default containerPort for mysql (3306)

  - DB_SERVER env var value is the PRIMARY (read-write) StatefulSet's svc URL provided as a result of the `helm install` cmd, W/O the port number: `mysql-primary.default.svc.cluster.local`.
    - We use the primary, not the secondary, b/c the Java app will need to connect to the main pod/ DB that allows reads AND writes.
    - Value must be [FQDN (Fully Qualified Domain Name)](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/), not the JDBC string in the `DatabaseConfig.java` file: `jdbc:mysql://localhost:3306/dbname`. The file comments that DB_SERVER's value is the `db host name, like localhost without the port`.
    - Per ChatGPT, `.svc.cluster.local` suffix is the default DNS domain for services within a Kubernetes cluster. When you reference a service by its name without the namespace or domain suffix, Kubernetes automatically appends `.svc.cluster.local` to the service name to resolve it within the cluster. This ensures that your application can correctly resolve and connect to services using just their names.

- Uninstall cmd: `helm uninstall mysql`.
  - On every `uninstall`, ensure you clean up pvc's AND pv's:
    - `kubectl get pvc` and `kubectl get pv`
    - `kubectl delete pvc [pvc1 name] [pvc2 name] ...` and `kubectl delete pv [pv1 name] [pv2 name] ...`
    - Delete on Linode > Volumes UI as well.
    - Esp crucial for primary DB, since it's using `linode-block-storage-retain` (i.e. deleting pvc's will not delete underlying pv's)

EXERCISE 3: Deploy your [Java Application](https://gitlab.com/twndevops/kubernetes-exercises) with 2 replicas

- Built Java-Gradle app (`bootcamp-docker-java-mysql-project`) into JAR file: `gradle clean build`.

- Ran `java -jar bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar` locally to see port where app server is running (i.e. `containerPort` for deployment component, 8080).

- Created new public repo in DockerHub: `upnata/bootcamp-docker-java-mysql-project`.

- Created [Dockerfile](https://gitlab.com/twndevops/kubernetes-exercises/-/blob/main/Dockerfile), built app image, and pushed to DockerHub repo.

  - Ran the image locally as a container to check for any errors before trying to deploy it in the cluster!

  - Pushed to Docker repo:

    - `docker build -t upnata/bootcamp-docker-java-mysql-project:1.0-SNAPSHOT .`
    - `docker push upnata/bootcamp-docker-java-mysql-project:1.0-SNAPSHOT`

  - Java app image built from the Dockerfile works w/ or w/o ENV vars. K8s manifest or Docker Compose file env var values overwrite ENV var values set in the Dockerfile. The Dockerfile values apply to the cluster only if they're not overwritten.

- Dockerfile errors debugged:

1. App was throwing `Error: A JNI error has occurred, please check your installation and try again Exception in thread "main" java.lang.UnsupportedClassVersionError: org/springframework/boot/loader/JarLauncher has been compiled by a more recent version of the Java Runtime (class file version 61.0), this version of the Java Runtime only recognizes class file versions up to 52.0.`

- This meant the Java runtime inside your Docker container is older than the version used to compile the Java application. The class file version 61.0 corresponds to Java 17, while class file version 52.0 corresponds to Java 8.
- FIX: Used a base image that supports Java 17.

2. Post-deploy to cluster, the 2 app replicas always had a Status of CrashLoopBackOff.

- LEARNING: Can monitor pod events and debug using `kubectl describe pod [pod name]`. Can also debug using `kubectl logs pod [pod name]`.

- FIX: MUST DO A MULTI-ARCH BUILD! Linode runs amd64 images, not arm64, which is why images broke immediately when run in the pod.

  - `docker buildx build --push \
--platform linux/amd64,linux/arm64 \
--tag upnata/bootcamp-docker-java-mysql-project:1.0-SNAPSHOT .`
  - This cmd builds and pushes the image for specified architectures, but the images won't be available locally.

- Since Java app Deployment depends on `proj-secret` and `proj-configmap`, deploy both:

  1. manually first: `kubectl apply -f secret.yaml` and `kubectl apply -f configmap.yaml` OR
  2. w/ Java app: `kubectl apply -f java-gradle-app.yaml`. Include Secret and ConfigMap configs BEFORE Deployment in the file (ORDER OF COMPONENT CREATION MATTERS!)

  - `mysql` chart install created 1 secret! See w/ `kubectl get secret` and output secret using `kubectl get secret mysql -o yaml`.

    - Configured Java app and phpmyadmin UI to use MySQL's root password and custom DB user password from this Secret.

  - `mysql` chart install created 2 cm's! See w/ `kubectl get cm` and output cm's using `kubectl get cm mysql-primary -o yaml` and `kubectl get cm mysql-secondary -o yaml`.

UNRESOLVED QUESTION/ISSUE: Java app UI doesn't render DB data.

EXERCISE 4: Deploy phpmyadmin MySQL UI (1 replica for own use, doesn't need to be highly available)

- `kubectl apply -f phpmyadmin.yaml`. Full setup is running in cluster!

EXERCISE 5: Deploy Ingress Controller in the cluster using Helm, so users can access Java app using domain name (not IP)

- Found K8s-managed [Helm chart for ingress-nginx](https://artifacthub.io/packages/helm/ingress-nginx/ingress-nginx) on ArtifactHub.
  - Add chart repo locally: `helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx` and `helm repo update`
  - Install: `helm install java-ingress-nginx ingress-nginx/ingress-nginx --set controller.publishService.enabled=true`
    - `--set` flag allocates public (LoadBalancer) IP for Ingress address to use w/ Nginx
    - Check status of controller using EITHER `kubectl get svc -o wide -w java-ingress-nginx-controller` OR `kubectl get pod` (recall: controller is a pod)
      - controller svc is of LoadBalancer type, accessible externally
    - Check Linode provisioned a NodeBalancer = cluster entrypoint, offers browser-accessible IP (w/ HTTP/HTTPS ports), forwards reqs to Ingress controller pod which uses configured rules to route reqs to cluster-internal svc's

EXERCISE 6: Create Ingress rule for Java app access (NOT phpmyadmin)

- Started w/ boilerplate output by controller chart install cmd.
- As the sole entrypoint to the cluster, used NodeBalancer host name for `host`.
- Configured rule to forward HTTP requests from NodeBalancer to Internal `java-gradle-app-svc`.

EXERCISE 7: Port-forward for phpmyadmin svc to be accessible on localhost (for security reasons, won't expose phpmyadmin externally)

- Ran `kubectl port-forward service/phpmyadmin-svc :80`,

  - `kubectl` finds/ allocates unused host port.
  - Can access phpmyadmin UI from browser by visiting either `http://127.0.0.1:[assigned port]/` or `http://localhost:[assigned port]/`.

  - Logged in to MySQL UI w/ root user creds or custom user creds.
  - Clicked Databases > check `mysqldb` DB exists.

- Referenced following K8s port forwarding docs:
  - [Forward a local port to a port on the Pod](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/#forward-a-local-port-to-a-port-on-the-pod)
  - [Optionally let kubectl choose the local port](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/#let-kubectl-choose-local-port)

EXERCISE 8: Create Helm Chart for Java App including all app config files (deployment, service, secret, configmap, ingress).

- Add Helm chart dir: `helm create [chart name]`.

- B/c there's only 1 app, no need for values overrides files (like in the microservices demo, there were 10); just values.yaml is sufficient in this case.

- To test templates populate default values: `helm template java-gradle-app`.

- After clearing all chart and phpmyadmin components from cluster, installed chart using `helm install java-app java-gradle-app`. `java-app`= release name, `java-gradle-app`= chart name.

  - `helm ls` to confirm `java-app` chart is running.

- Checked app is still accessible via browser using NodeBalancer host name.

  - ISSUE: Java app UI doesn't render DB data.
    - One way to test if Deployment env vars are reading the default values correctly is to deploy phpmyadmin using the templates secret and configmap (`java-gradle-app-secret` and `java-gradle-app-configmap`) rather than project secret and configmap (`proj-secret` and `proj-configmap`).
      - TEST RESULT: phpmyadmin env vars read templated secret and configmap data. Was able to login to UI using root AND nonroot user creds, and team_members table was accessible both times.

- Ran `helm uninstall java-app` to uninstall chart.

- Ran `helmfile sync` to sync cluster w/ desired state configured in `java-gradle-app` chart.

  - ISSUE: NodeBalancer host name `139-144-255-5.ip.linodeusercontent.com` loads app. However, external IP `139.144.255.5` shows `404 Not Found nginx`.

  - FOLLOW-UP AT LATER TIME: Compare [solution Helm chart templates for app deployment, secret, and configmap](https://gitlab.com/twn-devops-bootcamp/latest/10-kubernetes/kubernetes-exercises/-/tree/feature/solutions/java-app/templates?ref_type=heads), which use Helm ranges, w/ your own simpler mappings.

- Ran `helmfile destroy` to remove `java-app` release.
